####################
#Variables communes#
####################
company     = "structure"
prefix      = "str"
environment = "test"
location    = "northeurope"
description = "Déployer un serveur MySQL"
owner       = "Younes djebbouri"

##################
# Authentication #
##################
azure-subscription-id = "a completer"
azure-client-id       = "a completer"
azure-client-secret   = "a completer"
azure-tenant-id       = "a completer"

################
# MySQL Server #
################
mysql-admin-login    = "structure-admin"
mysql-admin-password = "Th1P@ssw0rd"
mysql-version        = "5.7"
mysql-sku-name       = "B_Gen5_1"
mysql-storage        = "5120"