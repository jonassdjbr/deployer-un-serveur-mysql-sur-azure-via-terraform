################################
## Variables du Azure Provider##
################################

# Azure authentication variables

variable "azure-subscription-id" {
  type        = string
  description = "Azure Subscription ID"
}

variable "azure-client-id" {
  type        = string
  description = "Azure Client ID"
}

variable "azure-client-secret" {
  type        = string
  description = "Azure Client Secret"
}

variable "azure-tenant-id" {
  type        = string
  description = "Azure Tenant ID"
}

#############################
## Application - Variables ##
#############################

# company name 
variable "company" {
  type        = string
  description = "Cette variable définit le nom de l'entreprise utilisé pour construire les ressources"
}

# company prefix 
variable "prefix" {
  type        = string
  description = "Cette variable définit le préfixe du nom de l'entreprise utilisé pour créer des ressources"
}

# azure region
variable "location" {
  type        = string
  description = "La région Azure où le groupe de ressources sera créé"
  default     = "east us"
}

# azure region shortname
variable "region" {
  type        = string
  description = "La région Azure où le groupe de ressources sera créé"
  default     = "eu"
}

# owner
variable "owner" {
  type        = string
  description = "Spécifiez le propriétaire de la ressource"
}

# description
variable "description" {
  type        = string
  description = "Fournir une description de la ressource"
}

# application or company environment
variable "environment" {
  type        = string
  description = "Cette variable définit l'environnement à construire"
  default     = "test"
}

########################################
# Azure Database for MySQL - Variables #
########################################

variable "mysql-admin-login" {
  type        = string
  description = "Le Login pour s'authentifier au serveur MySQL"
}

variable "mysql-admin-password" {
  type        = string
  description = "Le mot de passe pour s'authentifier au serveur MySQL"
}

variable "mysql-version" {
  type        = string
  description = "La version du serveur MySQL"
  default     = "8.0"
}

variable "mysql-sku-name" {
  type        = string
  description = "nom du MySQL SKU"
  default     = "B_Gen5_1"
}

variable "mysql-storage" {
  type        = string
  description = "Stockage MySQL en Mo, de 5120 MB a 4194304 MB"
  default     = "5120"
}